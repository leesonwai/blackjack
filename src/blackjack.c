/* blackjack.c
 *
 * Copyright 2020 Joshua Lee
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "blackjack.h"

#define MAXPLAYERS  3
#define MINPLAYERS  1
#define NPASSES     8192 
#define PROG        "blackjack" /* basename () unavailable on Windows */
#define SAVEFILE    "save.txt"

void    die           (void);
void    emitopts      (int op);
int     getmove       (struct player *p);
int     getopt        (void);
int     gettotal      (struct player *p);
void    pprint        (struct player *p);

/*
 * Writes a player's hand to file.
 */
void
hfprint (FILE         *fp,
         struct card **h,
         int           nh)
{
  fprintf (fp, "%d\n", h[0]->hole);

  for (int i = 0; i < nh; ++i)
    {
      for (struct card *c = h[i]; c; c = c->next)
        fprintf (fp, "%d%c", c->rank, (c->next) ? ' ' : '\n');
    }
}

/*
 * Recursive function that allocates, sets and adds a
 * card to a player's hand.
 */
struct card *
cardadd (struct card *h,
         int          rank)
{
  /*
   * If the node is empty, allocate, set and return a pointer.
   * Otherwise, recursively call cardadd () until the first
   * empty node is encountered.
   */
  if (!h)
    {
      if (!(h = malloc (sizeof (struct card))))
        die ();

      h->hole = 0;
      h->rank = rank;
      h->next = NULL;
    }
  else
    {
      h->next = cardadd (h->next, rank);
    }

  return h;
}

/*
 * Determines whether a player is bust.
 */
int
isbust (struct player *p)
{
  int nb = 0;

  for (int i = 0; i < p->nh; ++i)
    {
      if (p->h[i]->rank == BUST)
        ++nb;
    }

  return p->nh == nb;
}

/*
 * Returns the points value of a card.
 */
int
getpts (struct card *c,
        int          cur)
{
  switch (c->rank)
    {
    case 14:
      return (cur + 11 > BLACKJACK) ? 1 : 11;
    case 13:
    case 12:
    case 11:
    case 10:
      return 10;
    default:
      return c->rank;
    }
}

/*
 * Determines whether a player is holding an ace.
 */
int
hasace (struct player *p)
{
  for (int i = 0; i < p->nh; ++i)
    {
      for (struct card *c = p->h[i]; c; c = c->next)
        {
          if (c->rank == 14)
            return 1;
        }
    }

  return 0;
}

/*
 * Writes deck details to file.
 */
void
dfprint (FILE        *fp,
         struct deck *d)
{
  fprintf (fp, "%d %d\n", d->nc, d->pos);
  for (int i = 0; i < d->nc; ++i)
    fprintf (fp, "%d%c", d->list[i], (i == d->nc - 1) ? '\n' : ' ');
}

/*
 * Writes players' details to file.
 */
void
pfprint (FILE           *fp,
         struct player **p,
         int             np)
{
  for (int i = 0; i < np; ++i)
    {
      fprintf (fp, "%d %s %d\n", p[i]->st, p[i]->name, p[i]->nh);
      hfprint (fp, p[i]->h, p[i]->nh);
    }
}

/*
 * Maps a card's rank to a prettified card-like representation.
 */
char *
getface (struct card *c)
{
  if (c->hole)
    return "[/]";

  switch (c->rank)
    {
    case 2: return "[2]";
    case 3: return "[3]";
    case 4: return "[4]";
    case 5: return "[5]";
    case 6: return "[6]";
    case 7: return "[7]";
    case 8: return "[8]";
    case 9: return "[9]";
    case 10: return "[10]";
    case 11: return "[J]";
    case 12: return "[Q]";
    case 13: return "[K]";
    case 14: return "[A]";
    default: return "[?]"; /* Quietens the compiler warning */
    }
}

/*
 * Splits a player's opening hand.
 */
void
split (struct player *p)
{
  p->h[1] = p->h[0]->next;
  p->h[0]->next = NULL;
  ++p->nh;
}

/*
 * Prompts the user to choose whether to split their hand.
 */
int
issplit (struct player *p)
{
  int opt;

  printf ("%s can split hand\n", p->name);
  emitopts (OP_SPLIT);
  while ((opt = getopt ()) != YES && opt != NO);

  return opt;
}

/*
 * Determines whether a player can split their hand.
 */
int
cansplit (struct player *p)
{
  return (getpts (p->h[0], 0) == 10 && getpts (p->h[0]->next, 0) == 10) ||
          p->h[0]->rank == p->h[0]->next->rank;
}

/*
 * Determines whether a player has a blackjack.
 */
int
natcheck (struct player *p)
{
  int ret = 0;

  if (getpts (p->h[0], 0) + getpts (p->h[0]->next, 0) == BLACKJACK)
    p->st = ret = BJ;

  return ret;
}

/*
 * Loads a player's hand(s).
 */
void
hload (struct player *p,
       FILE          *fp,
       int            nh)
{
  unsigned int tmp;
  int hand = 0;
  int c;
  int rank;

  fscanf (fp, "%u\n", &tmp);

  while (hand < nh)
    {
      /*
       * The number of cards in each hand is unknown, thus they must
       * be parsed character by character.
       */
      while (isblank (c = getc (fp)));
      for (rank = 0; isdigit (c); c = fgetc (fp))
        rank = 10 * rank + (c - '0');
      p->h[hand] = cardadd (p->h[hand], rank);

      if (!hand)
        p->h[0]->hole = tmp;

      if (c == '\n') /* In file, hands are delimited by newlines. */
        ++hand;
      else if (c == EOF)
        break;
    }
}

/*
 * Determines whether the dealer should peek at the hole card.
 */
int
canpeek (struct card *h)
{
  return getpts (h->next, 0) == 10 || getpts (h->next, 0) == 11;
}

/*
 * Shuffles a deck.
 */
void
shuffle (struct deck *d)
{
  for (int i = 0, j, k, l; i < NPASSES; ++i)
    {
      j = rand () % d->nc;
      k = rand () % d->nc;

      l = d->list[j];
      d->list[j] = d->list[k];
      d->list[k] = l;
    }
}

/*
 * Deals a card to each of a player's active hands.
 */
void
deal (struct player *p,
      struct deck   *d)
{
  for (int i = 0; i < p->nh; ++i)
    {
      p->h[i] = cardadd (p->h[i], d->list[d->pos--]);

      if (!d->pos) /* Guard against deck exhaustion */
        {
          d->pos = d->nc - 1;
          shuffle (d);
        }
    }
}

/*
 * Frees a player and their hand(s).
 */
void
pfree (struct player *p)
{
  struct card *c;

  for (int i = 0; i < p->nh; ++i)
    {
      while (p->h[i]) /* Frees the linked list */
        {
          c = p->h[i];
          p->h[i] = p->h[i]->next;
          free (c);
        }
    }
}

/*
 * Prints the winner(s) of the game.
 */
void
printwin (struct game *g)
{
  int type = -1; /* The type of win, BJ has preference. */
  int nw = 0;

  for (int i = 0; i < g->np; ++i)
    {
      if (g->p[i]->st == BJ)
        type = BJ;
    }

  for (int i = 0; type != BJ && i < g->np; ++i)
    {
      if (g->p[i]->st == HI)
        type = HI;
    }

  for (int i = 0; type != BJ && type != HI && i < g->np; ++i)
    {
      if (g->p[i]->st == DEF)
        type = DEF;
    }

  for (int i = 0; i < g->np; ++i)
    {
      if (g->p[i]->st == type)
        ++nw;
    }

  printf ("%s", (nw > 1) ? "Draw between\n" : "Winner: ");
  for (int i = 0; i < g->np; ++i)
    {
      if (g->p[i]->st == type)
        printf ("%s (%d)\n", g->p[i]->name, gettotal (g->p[i]));
    }
}

/*
 * Returns the total points value of the player's hand(s).
 */
int
gettotal (struct player *p)
{
  int pts[MAXHANDS] = { 0 };
  int hi;

  for (int i = 0; i < p->nh; ++i)
    {
      for (struct card *c = p->h[i]; c; c = c->next)
        pts[i] += getpts (c, pts[i]);
    }

  for (int i = 0; i < p->nh; ++i)
    {
      if (!i || pts[i] > pts[i - 1]) /* This shouldn't explode. */
        hi = pts[i];
    }

  return hi;
}

/*
 * Checks the value of the player's current hand(s) and sets their
 * status accordingly.
 */
void
pcheck (struct player *p)
{
  int pts = 0;

  for (int i = 0; i < p->nh; ++i)
    {
      for (struct card *c = p->h[i]; c; c = c->next)
        pts += getpts (c, pts);

      if (pts > BLACKJACK)
        p->h[i]->rank = BUST;

      pts = 0;
    }

  if (isbust (p))
    {
      p->st = BUST;
      printf ("%s bust\n", p->name);
    }
}

/*
 * In a non-blackjack game, determines the player with the highest
 * score and records them as the winner.
 */
void
sethi (struct game *g)
{
  int pts;
  int hi;

  for (int i = 0; i < g->np; ++i)
    {
      if (!isbust (g->p[i]) && ((pts = gettotal (g->p[i])) > hi || !i))
        hi = pts;
    }

  for (int i = 0; i < g->np; ++i)
    {
      if (gettotal (g->p[i]) >= hi)
        g->p[i]->st = HI;
    }
}

/*
 * Sets all non-bust players as winners in the case of the dealer
 * going bust.
 */
void
setall (struct game *g)
{
  for (int i = 1; i < g->np; ++i)
    {
      if (!isbust (g->p[i]))
        g->p[i]->st = DEF;
    }
}

/*
 * Processes the dealer's turn.
 */
void
dealerproc (struct game *g)
{

  int pts;

  printf ("Dealer reveals the hole card\n");
  g->p[0]->h[0]->hole = 0;
  pprint (g->p[0]);

  if ((pts = gettotal (g->p[0])) < 17 || (pts == 17 && hasace (g->p[0])))
    {
      printf ("Dealer draws a card\n");
      deal (g->p[0], g->d);
      pprint (g->p[0]);
    }

  pcheck (g->p[0]);
}

/*
 * Writes game status to file.
 */
void
save (struct game *g)
{
  FILE *fp;

  if (!(fp = fopen (SAVEFILE, "w")))
    die ();

  fprintf (fp, "%d %d\n", g->pos, g->np);
  pfprint (fp, g->p, g->np);
  dfprint (fp, g->d);

  fclose (fp);
}

/*
 * Prints a player's hand(s).
 */
void
pprint (struct player *p)
{
  printf ("%s%s", p->name, (p->nh > 1) ? "\n" : ": ");
  for (int i = 0; i < p->nh; ++i)
    {
      if (p->nh > 1)
        printf ("Hand %d: ", i);

      for (struct card *c = p->h[i]; c; c = c->next)
        printf ("%s%c", getface (c), (c->next) ? ' ' : '\n');
    }
}

/*
 * Deal's card(s) to a players hand(s) until player wins, busts or stops.
 */
void
hit (struct player *p,
     struct deck   *d)
{
  do
    {
      deal (p, d);
      pprint (p);
      pcheck (p);
    }
  while (!isbust (p) && getopt () == HIT);
}

/*
 * Get's a player's move choice.
 */
int
getmove (struct player *p)
{
  int move;

  printf ("%s\n", p->name);
  while ((move = getopt ()) < 0 || move >= NMOVES);

  return move;
}

/*
 * Determines if a game is in a won state.
 */
int
iswon (struct game *g)
{

  int nb = 0;

  for (int i = 0; i < g->np; ++i)
    {
      if (g->p[i]->st == BJ)
        return 1;
    }

  for (int i = 0; i < g->np; ++i)
    {
      if (isbust (g->p[i]))
        ++nb;
    }

  return nb == g->np - 1; /* The game is won if all but one player is bust */
}

/*
 * Prints option list for player.
 */
void
emitopts (int op)
{
  switch (op)
    {
    case OP_NEW:
      printf ("%d: New\n%d: Load\n", NEW, LOAD);
      break;
    case OP_P:
      printf ("Players (%d to %d)\n", MINPLAYERS, MAXPLAYERS);
      break;
    case OP_D:
      printf ("Decks (1, 2 or 4)\n");
      break;
    case OP_SPLIT:
      printf ("%d: Yes\n%d: No\n", YES, NO);
      break;
    case OP_MOVE:
      printf ("%d: Stand\n%d: Hit\n%d: Save\n%d: Quit\n", STAND, HIT, SAVE, QUIT);
      break;
    }
}

/*
 * Performs initial set-up and checks for a new game.
 */
void
setup (struct game *g)
{
  for (int i = 0; i < g->np; ++i)
    {
      deal (g->p[i], g->d);
      if (!i)
        g->p[i]->h[0]->hole = 1;
      deal (g->p[i], g->d);
    }

  printf ("Initial deal...\n");
  for (int i = 0; i < g->np; ++i)
    pprint (g->p[i]);

  if (canpeek (g->p[0]->h[0]))
    {
      printf ("Dealer peeks at the hole card...\n");
      if (natcheck (g->p[0]))
        {
          g->p[0]->h[0]->hole = 0;
          printf ("%s has blackjack\n", g->p[0]->name);
        }
      else
        {
          printf ("%s doesn't have blackjack\n", g->p[0]->name);
        }
    }

  for (int i = 1; i < g->np; ++i)
    {
      if (natcheck (g->p[i]))
        printf ("%s has blackjack\n", g->p[i]->name);
    }

  for (int i = 1; !iswon (g) && i < g->np; ++i)
    {
      if (cansplit (g->p[i]) && issplit (g->p[i]))
        {
          split (g->p[i]);
          deal (g->p[i], g->d);
          pprint (g->p[i]);
        }
    }
}

/*
 * Allocates, populates and returns a deck from file.
 */
struct deck *
dload (FILE *fp)
{
  struct deck *d;

  if (!(d = malloc (sizeof (struct deck))))
    die ();

  fscanf (fp, "%d %d\n", &d->nc, &d->pos);

  if (!(d->list = malloc (sizeof (int) * d->nc)))
    die ();

  for (int i = 0; i < d->nc; ++i)
    fscanf (fp, "%d", &d->list[i]);

  return d;
}

/*
 * Allocates, sets and returns a game's players from file.
 */
struct player **
pload (FILE *fp,
       int   np)
{
  struct player **p;

  if (!(p = malloc (sizeof (struct player *) * np)))
    die ();

  for (int i = 0; i < np; ++i)
    {
      if (!(p[i] = malloc (sizeof (struct player))))
        die ();

      fscanf (fp, "%d %s %d\n", &p[i]->st, p[i]->name, &p[i]->nh); 
      for (int j = 0; j < MAXHANDS; ++j)
        p[i]->h[j] = NULL;
      hload (p[i], fp, p[i]->nh);
    }

  return p;
}

/*
 * Allocates, populates and returns a randomly ordered deck.
 */
struct deck *
dalloc (int nd)
{
  struct deck *d;

  if (!(d = malloc (sizeof (struct deck))))
    die ();

  if (!(d->list = malloc (sizeof (int) * nd * DECKSIZ)))
    die ();

  d->nc = nd * DECKSIZ;
  d->pos = nd * DECKSIZ - 1;
  for (int i = 0; i < d->nc; ++i)
    d->list[i] = (i % NRANKS) + 2;

  shuffle (d);

  return d;
}

/*
 * Allocates, sets and returns a new game's players.
 */
struct player **
palloc (int np)
{
  struct player **p;

  if (!(p = malloc (sizeof (struct player *) * np)))
    die ();

  for (int i = 0; i < np; ++i)
    {
      if (!(p[i] = malloc (sizeof (struct player))))
        die ();

      p[i]->st = NONE;
      snprintf (p[i]->name, MAXNAME, (!i) ? "Dealer" : "Player-%d", i);
      p[i]->nh = 1;

      for (int j = 0; j < MAXHANDS; ++j)
        p[i]->h[j] = NULL;
    }

  return p;
}

void
die (void)
{
  perror (PROG);
  exit (EXIT_FAILURE);
}

/*
 * Function that displays a customisable prompt and returns
 * integer input.
 */
int
getopt ()
{
  int c;

  printf ("> ");
  while ((c = getchar ()) == '\n');

  return c - '0';
}

/*
 * Frees the game.
 */
void
gfree (struct game *g)
{
  free (g->d->list);
  free (g->d);
  for (int i = 0; i < g->np; ++i)
    pfree (g->p[i]);
  free (g);
}

/*
 * Runs the game until completion or user exit.
 */
void
run (struct game *g,
     int          mode)
{
  if (mode == NEW) /* A fresh game must be set-up */
    setup (g);

  if (mode == LOAD) /* A loaded game must be displayed */
    {
      printf ("Game loaded succesfully\n");
      for (int i = 0; i < g->np; ++i)
        pprint (g->p[i]);
    }

  if (!iswon (g))
    emitopts (OP_MOVE);

  while (!iswon (g) && g->pos < g->np)
    {
      switch (getmove (g->p[g->pos]))
        {
          case STAND:
            pprint (g->p[g->pos]);
            break;
          case HIT:
            hit (g->p[g->pos], g->d);
            break;
          case SAVE:
            save (g);
            printf ("Game saved successfully\n");
            return;
          case QUIT:
            return;
        }
      ++g->pos;
    }

  dealerproc (g);
  if (isbust (g->p[0]))
    setall (g);
  else
    sethi (g);

  printwin (g);
}

/*
 * Allocates, sets and returns a game from save file.
 */
struct game *
load (void)
{
  FILE *fp;
  struct game *g;

  if (!(fp = fopen (SAVEFILE, "r")))
    die ();

  if (!(g = malloc (sizeof (struct game))))
    die ();

  fscanf (fp, "%d %d\n", &g->pos, &g->np);
  g->p = pload (fp, g->np);
  g->d = dload (fp);

  fclose (fp);

  return g;
}

/*
 * Allocates, sets and returns a new game.
 */
struct game *
galloc (int np,
        int nd)
{
  struct game *g;

  if (!(g = malloc (sizeof (struct game))))
    die ();

  g->pos = 1;
  g->np = np;
  g->p = palloc (np);
  g->d = dalloc (nd);

  return g;
}

/*
 * Sets the game parameters from user input.
 */
void
setopts (int *mode,
         int *np,
         int *nd)
{
  emitopts (OP_NEW);
  while ((*mode = getopt ()) != NEW && *mode != LOAD);

  if (*mode == LOAD)
    return;

  emitopts (OP_P);
  while ((*np = getopt ()) < MINPLAYERS || *np > MAXPLAYERS);
  ++*np; /* Dealer is also a player */

  emitopts (OP_D);
  while ((*nd = getopt ()) != 1 && *nd != 2 && *nd != 4);
}

int
main (int   argc,
      char *argv[])
{
  int mode;
  int np;
  int nd;
  struct game *g;

  errno = 0;
  srand (time (NULL));
  setopts (&mode, &np, &nd);

  g = (mode == NEW) ? galloc (np, nd) : load ();
  run (g, mode);

  gfree (g);

#if defined (_WIN32) || defined (_WIN64)
  while (getchar () != EOF);
#endif

  return EXIT_SUCCESS;
}
