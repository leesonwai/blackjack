/* blackjack.h
 *
 * Copyright 2020 Joshua Lee
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#define BLACKJACK   21
#define DECKSIZ     52
#define MAXHANDS    2
#define MAXNAME     16
#define NRANKS      13

enum
{
  NO,
  YES
};

enum
{
  NONE,
  BUST,
  HI,
  BJ,
  DEF
};

enum
{
  STAND,
  HIT,
  SAVE,
  QUIT,

  NMOVES
};

enum
{
  NEW,
  LOAD
};

enum
{
  OP_NEW,
  OP_P,
  OP_D,
  OP_SPLIT,
  OP_MOVE
};

struct card
{
  unsigned int  hole : 1;       /* Whether the card is the hole */
  int           rank;           /* The card's rank */
  struct card  *next;           /* Pointer to next card in hand */
};

struct deck
{
  int  nc;                      /* Number of total cards */
  int  pos;                     /* Current position in deck list */
  int *list;                    /* Deck itself */
};

struct player
{
  int          st;              /* Player's stats (bust, etc.) */
  char         name[MAXNAME];   /* Player's display name */
  int          nh;              /* Number of active hands */
  struct card *h[MAXHANDS];     /* Player's hands */
};

struct game
{
  int             pos;          /* Current position at table */
  int             np;           /* Number of players */
  struct player **p;            /* Game's players */
  struct deck    *d;            /* Game's deck */
};
