# blackjack
### A tedious game of blackjack
![License: GPL3](https://img.shields.io/badge/license-GPL3-green?style=flat-square)

Written for a second year C assigment.

## Installing

```
meson builddir
cd builddir/
ninja
sudo ninja install
```
